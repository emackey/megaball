Ed Mackey, June 2012
https://twitter.com/emackey

This program, MegaBoards.cpp, is a tiny C program I wrote in May 2000, and
then emailed to Michael P Welch, author of "Scorched Tanks" for the Amiga
and DX-Ball & Super DX-Ball for the PC.  The goal was to allow MegaBall board
sets to be imported into DX-Ball.

The one source file contains both a function for loading the boards, and a
"main" program as an example command-line utility to convert boards to
ASCII art.  Both are sprinkled with comments on usage, which is the only
documentation for this.  Otherwise the code itself is unreadable, as all
the variables are named after 680x0 CPU registers, and the code was translated
by hand from the original BoardEd.ASM source.  I think I had forgotten so much
68k by year 2000 that I had to Google some of the opcodes.

In any case, I'm now releasing this code along with MegaBall itself to the
open source community.  Perhaps the old MegaBall boards will appear again
in someone's future project.  If that's you, please credit the author of any
board sets you use, if the author is known, or include a link back to the
MegaBall open-source repository here.

Super DX-Ball is distributed by BlitWise Productions:
http://www.blitwise.com/superdxb.html

Mike can be reached on the forums there, and on Twitter:
http://forums.blitwise.com/
https://twitter.com/mikew_blitwise
