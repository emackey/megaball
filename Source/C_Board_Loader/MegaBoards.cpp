// MegaBoards converter for old MegaBall boards.
// by Ed Mackey, 5/7/2000.

// Here's how it works:  There are 50 boards, each with
// 20 columns and 21 rows.  The top row of each board is
// always empty (all 0x00's) because that's where the game
// score and stuff goes on the screen (yeah, don't ask...).
// Entire boards are allowed to be empty, this means they
// don't exist and should be skipped by the game.

// So, allocate the correct amount of memory (given by the
// macro Whole_Thing below), and pass a pointer to that mem
// along with the filename you want to load into DecodeBoards().
// It will load the file and populate the memory.  It returns
// a char pointer to an error message if it failed (you know
// it worked if it returns NULL).

// Once you get back your block of memory, you can do what you
// like with it.  The example main() program below prints it
// out as a big ASCII text file.

// See the BrickList.txt file for a description of which byte
// values correspond to which bricks.

#include <stdio.h>
#include <stdlib.h>

// Size of uncompressed, decrypted boards.
#define Whole_Thing  (50 * 20 * 21)

// Positions in load buffer.
#define LodSavHdr   0
#define Chk2        4
#define Chk1        8
#define LodSavBuf  12

/* Stupid byte-order junk!! */
/* This should fix it no matter which byte-order you use */
static unsigned long ReadLong(char *fromhere)
{
	unsigned long val = 0;
	int t;

	for (t = 0; t < 4; ++t)
	{
		val = (val << 8) | (unsigned long)(*(unsigned char *)(fromhere++));
	}
	return(val);
}

static void WriteLong(char *tohere, long val)
{
	tohere[0] = (char)(val >> 24);
	tohere[1] = (char)(val >> 16);
	tohere[2] = (char)(val >>  8);
	tohere[3] = (char)(val);
}

// This was converted by hand from 68k assembly language.
// That means the variable names are just the CPU register names.
// So don't blame me if it's unreadable!  :)
char *DecodeBoards(char *name, char *Boards)
{
	int LoadSizeD6, D0, D3, D4, D5;
	char LoadBuf[21012], *LdBuf, *a2, *a3;
	FILE *infile;

	infile = fopen(name, "rb");
	if (!infile)
	{
		return("Can't open input file.");
	}

	LoadSizeD6 = fread(LoadBuf, 1, 21012, infile);
	fclose(infile);

	if ((LoadBuf[0] == 'B') && (LoadBuf[1] == 'D') && (LoadBuf[2] == 'S'))
	{
		LoadSizeD6 -= 4;
		LdBuf = LoadBuf;
	}
	else
	{
		LdBuf = LoadBuf - 4;
	}

	a2 = Boards;

	if (LoadSizeD6 <= 20)
	{
		return("Corrupt board file (too small).");
	}

	LoadSizeD6 = (LoadSizeD6 - 8) >> 2;
	a3 = LdBuf + Chk1;
	D5 = (int)ReadLong(LdBuf + Chk2);

	for (D3 = LoadSizeD6; D3 >= 0; --D3)
	{
		D5 -= (int)ReadLong(a3);
		a3 += 4;
	}

	if ((D5 == 0) || (D5 == 0xad6c72e5))
	{
		return("Corrupt board file (bad checksum 2).");
	}

	a3 = LdBuf + Chk1;
	for (D3 = LoadSizeD6; D3 >= 0; --D3)
	{
		WriteLong(a3, D5 ^ (int)ReadLong(a3));
		a3 += 4;
	}

	D4 = 0;
	a3 = LdBuf + LodSavBuf;

	for (D3 = (LoadSizeD6 - 1); D3 >= 0; --D3)
	{
		D4 += (int)ReadLong(a3);
		a3 += 4;
	}

	if (D4 != (int)ReadLong(LdBuf + Chk1))
	{
		return("Corrupt board file (bad checksum 1).");
	}

	D3 = 20999;
	a3 = LdBuf + LodSavBuf;
	D0 = 0;

	while (D3 >= 0)   // Run-length encoding decoder.
	{
		if (D0 == 0)
		{
			D0 = *(a3++);
		}
		else if (D0 < 0)
		{
			while (++D0 < 0)
			{
				*(a2++) = *a3;
				if (--D3 < 0)
					return NULL;
			}
			*(a2++) = *(a3++);
			if (--D3 < 0)
				return NULL;
		}
		else
		{
			--D0;
			*(a2++) = *(a3++);
			if (--D3 < 0)
				return NULL;
		}
	}

	return NULL;
}

//
// This main program shows how to use the DecodeBoards() piece of junk thingie.
//
void main(int argc, char *argv[])
{
	char *errMsg, *Boards, *brick, buffer[40], *buf;
	int boardNum, x, y;
	FILE *outfile;

	if (argc < 3)
	{
		printf("USAGE: %s <infile> <outfile>\n", argv[0]);
		exit(0);
	}

	// First, allocate memory to hold 50 boards.
	Boards = (char *)malloc(Whole_Thing);
	if (!Boards)
	{
		printf("Out of memory\n");
		exit(1);
	}

	// Then, call DecodeBoards() with the filename and the memory.
	errMsg = DecodeBoards(argv[1], Boards);
	if (errMsg)
	{
		// Give the user any error message that it returned, and abort.
		printf("%s\n", errMsg);
		free(Boards);
		exit(1);
	}

	// Finally, do something with the memory...  In this case, we
	// are going to print out an ASCII file with the contents of
	// all the boards.

	outfile = fopen(argv[2], "w");
	if (!outfile)
	{
		printf("Can't open output file\n");
		free(Boards);
		exit(1);
	}

	brick = Boards; // look at the first brick.
	for (boardNum = 1; boardNum <= 50; ++boardNum)  // 50 boards.
	{
		fprintf(outfile, "\nBoard %d:\n", boardNum);

		for (y = 0; y < 21; ++y)   // 21 rows per board.
		{
			buf = buffer;
			for (x = 0; x < 20; ++x)  // 20 columns per board
			{
				*(buf++) = *(brick++) + ' ';
			}
			*buf = 0;
			fprintf(outfile, "%s\n", buffer);
		}
	}

	fclose(outfile);
	free(Boards);

	exit(0);
}

// End of MegaBoards.cpp
